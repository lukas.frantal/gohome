## [1.2.0](https://gitlab.com/lukas.frantal/gohome/compare/v1.1.2...v1.2.0) (2021-05-07)


### Features

* update packages and setup ([a310fb3](https://gitlab.com/lukas.frantal/gohome/commit/a310fb3c86650be5a611e9d0814ba9af5f68b510))

### [1.1.2](https://gitlab.com/lukas.frantal/gohome/compare/v1.1.1...v1.1.2) (2021-03-09)


### Bug Fixes

* setup ts config, eslint ([7bac16c](https://gitlab.com/lukas.frantal/gohome/commit/7bac16c73982a4ae918d303cb76a5c6d84d05d42))

### [1.1.1](https://gitlab.com/lukas.frantal/gohome/compare/v1.1.0...v1.1.1) (2021-03-04)


### Bug Fixes

* readme docs ([830c422](https://gitlab.com/lukas.frantal/gohome/commit/830c422772869857eab37fc3040c42d84577c137))

## [1.1.0](https://gitlab.com/lukas.frantal/gohome/compare/v1.0.1...v1.1.0) (2021-03-04)


### Features

* new calculation and docs ([320cde1](https://gitlab.com/lukas.frantal/gohome/commit/320cde1044ecf8fddddd922479dba8aa0b93bfc0))

### [1.0.1](https://gitlab.com/lukas.frantal/gohome/compare/v1.0.0...v1.0.1) (2021-03-03)


### Bug Fixes

* doc readme ([f693ac8](https://gitlab.com/lukas.frantal/gohome/commit/f693ac8cb1a01df83132dfa9a56a7447f9dc08a2))

## 1.0.0 (2021-03-03)


### Features

* init package ([2eb60fb](https://gitlab.com/lukas.frantal/gohome/commit/2eb60fbf6eb83532c3096cebfa2688028d0aa6b7))
