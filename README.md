# gohome

This project can calculate your actual work time and show you when you can go home. Your first run will save your start working time and the next run will load time from the saved previous time.

# [demo](https://gohome.lukasfrantal.com)

[![coverage report](https://gitlab.com/lukas.frantal/gohome/badges/feat/init-package/coverage.svg)](https://gitlab.com/lukas.frantal/gohome/-/commits/feat/init-package)
![npm](https://img.shields.io/npm/dt/gohome)
![npm](https://img.shields.io/bundlephobia/min/gohome/latest)
![npm](https://img.shields.io/npm/l/gohome)

<hr>

## Installation:

```
npm i gohome -g
```

## Example running:

```
gohome 8:00
```

```
gohome 08:00
```

```
gohome 12:11
```

## You can use this package for your next solution like this:

```js
const { gohome } = require('gohome')
const output = gohome(
	'8:00', // timeStartWork,
	8, // timeWorkMaxHours,
	6, // timeWorkMinHours,
	30, // timeLaunchMinutes
)
```

## If you want define your current hours and minutes it is possible like this:

```js
const { gohome } = require('gohome')
const output = gohome(
	'8:00', // timeStartWork,
	8, // timeWorkMaxHours,
	6, // timeWorkMinHours,
	30, // timeLaunchMinutes,
	10, // actual hours,
	0, // actual minutes,
)
```

## If you want check your start working time before running gohome function you can use this:

```js
const { gohome, isValidInput } = require('gohome')
const timeStartWork = '8:00'
const isValid = isValidInput(timeStartWork)

if (isValid) {
	gohome(
		timeStartWork, // timeStartWork,
		8, // timeWorkMaxHours,
		6, // timeWorkMinHours,
		30, // timeLaunchMinutes
	)
} else {
	console.error(`This start working time is not valid: ${timeStartWork}`)
}
```

## Expected incorrect result will be show in your console like this:

![alt 1](https://gitlab.com/lukas.frantal/gohome/-/raw/master/images/1.png)<br>

## Expected correct result if you defined correct start working time:

![alt 2](https://gitlab.com/lukas.frantal/gohome/-/raw/master/images/2.png)<br>

## Expected correct result if you worked 6 hours:

![alt 3](https://gitlab.com/lukas.frantal/gohome/-/raw/master/images/3.png)<br>

## Expected correct result if you worked more than 8 hours:

![alt 4](https://gitlab.com/lukas.frantal/gohome/-/raw/master/images/4.png)<br>
