module.exports = {
	presets: [
		'@babel/preset-env',
		'@babel/preset-typescript',
		[
			'minify',
			{
				builtIns: false,
			},
		],
	],
	plugins: ['@babel/proposal-class-properties', '@babel/proposal-object-rest-spread'],
}
