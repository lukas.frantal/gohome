// Needed variables in gtilab CI are NPM_TOKEN and GITLAB_TOKEN
// https://github.com/semantic-release/semantic-release/blob/master/docs/usage/ci-configuration.md#authentication

module.exports = {
	debug: true,
	repositoryUrl: 'git@gitlab.com:lukas.frantal/gohome.git',
	plugins: [
		// analyze commits with conventional-changelog
		[
			'@semantic-release/commit-analyzer',
			{
				preset: 'conventionalcommits',
			},
		],
		// generate changelog content with conventional-changelog
		[
			'@semantic-release/release-notes-generator',
			{
				preset: 'conventionalcommits',
			},
		],
		// updates the changelog file
		'@semantic-release/changelog',
		['@semantic-release/npm', { npmPublish: true, private: false }],
		// creating a git tag
		[
			'@semantic-release/gitlab',
			{
				gitlabUrl: 'https://gitlab.com/',
			},
		],
		// creating a new version commit
		'@semantic-release/git',
	],
}
