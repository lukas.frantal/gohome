#!/usr/bin/env node

import fs from 'fs'
import os from 'os'
import path from 'path'
import { gohome, gohomeI, isValidInput } from './main'

interface OutputI {
	visible: boolean
	color: string
	text: string
}

const loadOutput = (data: OutputI[]): void => {
	data.length > 0 && data.map((item: OutputI) => item.visible && console.log(item.color, item.text))
}

const showOutput = (time: gohomeI | string): void => {
	const yellowColor = '\x1b[33m%s\x1b[0m'
	const redColor = '\x1b[31m%s\x1b[0m'
	const greenColor = '\x1b[32m%s\x1b[0m'
	let output

	if (typeof time !== 'string') {
		output = [
			{
				visible: true,
				color: yellowColor,
				text: `Actual time: ${time.actual.hours}:${time.actual.minutes}`,
			},
			{
				visible: true,
				color: yellowColor,
				text: `Your start working time: ${time.start.hours}:${time.start.minutes}`,
			},
			{
				visible: true,
				color: yellowColor,
				text: '--------------------------------------',
			},
			{
				visible: true,
				color: time.min.done ? redColor : greenColor,
				text: 'You can go home by min time at: ' + `${time.min.hours}:${time.min.minutes}`,
			},
			{
				visible: time.min.done,
				color: redColor,
				text: 'Your actual saldo for min time is: ' + `- ${time.min.saldo.hours}:${time.min.saldo.minutes}`,
			},
			{
				visible: true,
				color: time.max.done ? redColor : greenColor,
				text: 'You can go home by max time at: ' + `${time.max.hours}:${time.max.minutes}`,
			},
			{
				visible: true,
				color: time.max.done ? redColor : greenColor,
				text: `Your actual saldo is: ${time.max.done ? '- ' : ''}` + `${time.max.saldo.hours}:${time.max.saldo.minutes}`,
			},
			{
				visible: true,
				color: yellowColor,
				text: '--------------------------------------',
			},
		]
	} else {
		output = [
			{
				visible: true,
				color: redColor,
				text: `This input: "${time}" is wrong,` + '\nplease define correct your start working time!',
			},
			{
				visible: true,
				color: yellowColor,
				/* eslint max-len: "off" */
				text: '--------------------------------------' + '\nCorrectly defined commands:' + '\ngohome 8:15' + '\ngohome 08:11' + '\ngohome 11:12',
			},
		]
	}

	loadOutput(output)
}

const newTimeStartWork: string | undefined = process.argv[2] || undefined

let data = {
	timeStartWork: newTimeStartWork,
	timeWorkMaxHours: 8,
	timeWorkMinHours: 6,
	timeLaunchMinutes: 30,
}

type DataI = typeof data

const resolvePath = (inputPath: string): string => path.resolve(__dirname, inputPath)
const pathConfigFolder: string = resolvePath(`${os.tmpdir()}/`)
if (!fs.existsSync(pathConfigFolder)) {
	fs.mkdirSync(pathConfigFolder)
}

const pathConfigJsonFile: string = resolvePath(`${os.tmpdir()}/gohome.json`)

if (fs.existsSync(pathConfigJsonFile)) {
	/* eslint @typescript-eslint/no-var-requires: "off", @typescript-eslint/no-unsafe-assignment: "off" */
	const config: DataI = require(pathConfigJsonFile)
	if (JSON.stringify(data, null, 2) === JSON.stringify(config, null, 2) || data.timeStartWork === undefined) {
		data = config
	} else {
		if (isValidInput(data.timeStartWork)) {
			fs.writeFile(pathConfigJsonFile, JSON.stringify(data, null, 2), err => {
				if (err) throw err
			})
		}
	}
} else {
	if (data.timeStartWork && !isValidInput(data.timeStartWork)) {
		showOutput(data.timeStartWork)
	}
	fs.writeFile(pathConfigJsonFile, JSON.stringify(data, null, 2), err => {
		if (err) throw err
	})
}
if (data.timeStartWork) {
	const time: false | gohomeI = gohome(data.timeStartWork, data.timeWorkMaxHours, data.timeWorkMinHours, data.timeLaunchMinutes)
	time ? showOutput(time) : showOutput(data.timeStartWork)
}
