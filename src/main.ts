const isValidInput = (time: string): boolean => Boolean(/(^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$)\b/g.exec(time))

export interface gohomeI {
	start: {
		hours: string
		minutes: string
	}
	actual: {
		hours: string
		minutes: string
	}
	min: {
		done: boolean
		hours: string
		minutes: string
		saldo: {
			hours: string
			minutes: string
		}
	}
	max: {
		done: boolean
		hours: string
		minutes: string
		saldo: {
			hours: string
			minutes: string
		}
	}
}

const gohome = (
	timeStartWork: string,
	timeWorkMaxHours: number,
	timeWorkMinHours: number,
	timeLaunchMinutes: number,
	actualHours: number = new Date().getHours(),
	actualMinutes: number = new Date().getMinutes(),
): gohomeI | false => {
	if (!isValidInput(timeStartWork)) {
		return false
	}
	const splitTime: number[] = timeStartWork.split(':').map(Number)
	const startWorkHours: number = splitTime[0]
	const startWorkMinutes: number = splitTime[1]

	const fullActualTimeInMinutes: number = actualMinutes + actualHours * 60
	const fullStartWorkTimeInMinutes: number = startWorkMinutes + startWorkHours * 60

	const formatingTime = (time: number): string => {
		const value = String(time)
		if (value.includes('-') && value.length === 2) {
			return `0${value.replace('-', '')}`
		} else if (value.length === 1) {
			return `0${value}`
		}
		return value
	}

	const calcHours = (time: number): number => Math.floor(time / 60)
	const calcMinutes = (time: number): string => formatingTime(Math.round((time / 60 - Math.floor(time / 60)) * 60))

	const conditionByMaxTime = (startTime: number, actualTime: number, maxTime: number): boolean => {
		switch (true) {
			case actualTime === startTime:
				return true
			case actualTime > startTime:
				return actualTime - maxTime < startTime
			default:
				return false
		}
	}

	const timeMinMinutes: number = fullStartWorkTimeInMinutes + timeWorkMinHours * 60 + timeLaunchMinutes
	const timeMaxMinutes: number = fullStartWorkTimeInMinutes + timeWorkMaxHours * 60 + timeLaunchMinutes

	const minTimeDone: boolean = conditionByMaxTime(fullStartWorkTimeInMinutes, fullActualTimeInMinutes, 6 * 60)
	const maxTimeDone: boolean = conditionByMaxTime(fullStartWorkTimeInMinutes, fullActualTimeInMinutes, 8 * 60)

	const gohomeMinTimeHours: string = formatingTime(calcHours(timeMinMinutes) - (fullActualTimeInMinutes >= fullStartWorkTimeInMinutes ? 0 : 24))
	const gohomeMinTimeMinutes: string = calcMinutes(timeMinMinutes)

	const gohomeMaxTimeHours: string = formatingTime(calcHours(timeMaxMinutes) - (fullActualTimeInMinutes >= fullStartWorkTimeInMinutes ? 0 : 24))
	const gohomeMaxTimeMinutes: string = calcMinutes(timeMaxMinutes)

	const timeSaldoMinMinutes: number = Math.abs(fullActualTimeInMinutes - timeMinMinutes)
	const timeSaldoMaxMinutes: number = Math.abs(fullActualTimeInMinutes - timeMaxMinutes)

	const saldoMinHours: string = formatingTime(calcHours(timeSaldoMinMinutes))
	const saldoMinMinutes: string = calcMinutes(timeSaldoMinMinutes)

	const saldoMaxHours: string = formatingTime(
		calcHours(fullActualTimeInMinutes >= fullStartWorkTimeInMinutes ? timeSaldoMaxMinutes : fullActualTimeInMinutes - timeMaxMinutes + 24 * 60),
	)
	const saldoMaxMinutes: string = calcMinutes(timeSaldoMaxMinutes)

	return {
		start: {
			hours: formatingTime(startWorkHours),
			minutes: formatingTime(startWorkMinutes),
		},
		actual: {
			hours: formatingTime(actualHours),
			minutes: formatingTime(actualMinutes),
		},
		min: {
			done: minTimeDone,
			hours: gohomeMinTimeHours,
			minutes: gohomeMinTimeMinutes,
			saldo: {
				hours: saldoMinHours,
				minutes: saldoMinMinutes,
			},
		},
		max: {
			done: maxTimeDone,
			hours: gohomeMaxTimeHours,
			minutes: gohomeMaxTimeMinutes,
			saldo: {
				hours: saldoMaxHours,
				minutes: saldoMaxMinutes,
			},
		},
	}
}

export { gohome, isValidInput }
