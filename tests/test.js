import { gohome, isValidInput } from '../dist/main.js'

const validTime = value => {
	return String(value).length === 1 ? `0${value}` : value
}

describe('Testing gohome package', () => {
	const correctInputs = []
	for (let i = 0; i < 24; i++) {
		for (let j = 0; j < 60; j++) {
			correctInputs.push({
				timeStartWork: '6:00',
				timeWorkMaxHours: 8,
				timeWorkMinHours: 6,
				timeLaunchMinutes: 30,
				actualHours: validTime(i),
				actualMinutes: validTime(j),
			})
		}
	}

	correctInputs.map(input => {
		test(
			'\nTesting gohome function with correct inputs' +
				`\nStart working time: ${input.timeStartWork}` +
				`\nMax hours: ${input.timeWorkMaxHours}` +
				`\nMin hours: ${input.timeWorkMinHours}` +
				`\nLaunch time minutes: ${input.timeLaunchMinutes}` +
				`\nActual time: ${input.actualHours}:${validTime(input.actualMinutes)}\n`,
			() => {
				expect(
					gohome(
						input.timeStartWork,
						input.timeWorkMaxHours,
						input.timeWorkMinHours,
						input.timeLaunchMinutes,
						input.actualHours,
						input.actualMinutes,
					),
				).toMatchSnapshot()
			},
		)
	})

	const wrongInputs = ['-1:00', ':', '1', '01:1', '24:00', '10:61', 10, null, {}, [], undefined, NaN, ' ', '']

	wrongInputs.map(input =>
		test(`Testing gohome function with uncorrect inputs: ${input}`, () => {
			expect(gohome(input, 8, 6, 30, 8, 0)).toMatchSnapshot()
		}),
	)

	wrongInputs.map(input =>
		test(`Testing isValidInput function with uncorrect inputs: ${input}`, () => {
			expect(isValidInput(input)).toMatchSnapshot()
		}),
	)
})
