declare const isValidInput: (time: string) => boolean;
export interface gohomeI {
    start: {
        hours: string;
        minutes: string;
    };
    actual: {
        hours: string;
        minutes: string;
    };
    min: {
        done: boolean;
        hours: string;
        minutes: string;
        saldo: {
            hours: string;
            minutes: string;
        };
    };
    max: {
        done: boolean;
        hours: string;
        minutes: string;
        saldo: {
            hours: string;
            minutes: string;
        };
    };
}
declare const gohome: (timeStartWork: string, timeWorkMaxHours: number, timeWorkMinHours: number, timeLaunchMinutes: number, actualHours?: number, actualMinutes?: number) => gohomeI | false;
export { gohome, isValidInput };
